
#include <stdio.h>
#include <math.h>

	float a, b, nb, c, nc, x, f; // input values
	char z1, z2, rodiik;

int main() 
{
	printf("a: ");
	scanf("%f", &a); // scan a
	printf("b: ");
	scanf("%f", &b); // scan b
	printf("c: ");
	scanf("%f", &c); // scan c
	
	if(b>=0) {z1 = '+';nb = b;} else {z1 = '-'; nb = b * -1; }
	if(c>=0) {z2 = '+';nc = c;} else {z2 = '-'; nc = c * -1; }
	
	printf("eq:\t%.1f * x ^ 2 %c %.1f * x %c %.1f = 0\n", a,z1,nb,z2,nc); // output values
	
	f = b*b - 4*a*c;
	printf("d:\t%.3f\n", f);

	return 0;
}
